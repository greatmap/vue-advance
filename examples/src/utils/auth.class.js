/**
 * token 管理类
 * @description
 * 获取有效的 token 值
 */
import { getLocalData, setLocalData } from './local-storage';

export class Auth {
  name = '';
  constructor(tokenName) {
    this.name = tokenName;
  }

  /**
   * 获取 token
   * @returns {string/null} 如果有 token 并且未过期返回 token 值，否则返回 null
   */
  getToken() {
    const data = getLocalData(this.name);
    if (data && data.expires > Date.now()) {
      return data.token;
    } else {
      return null;
    }
  }

  /**
   * 设置 token
   * @param {object} token 设置 token 对象，例如：{token: 'token', expires: 'expires'}
   */
  setToken(token) {
    setLocalData(this.name, token);
  }
}

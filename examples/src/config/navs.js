const navs = [
  {
    name: '指令',
    children: [
      {
        path: '1-1.v-once',
        name: '文本插值的扩展：`v-once` 指令',
        component: () => import('@/views/1-1.v-once')
      },
      {
        path: '1-2.v-html',
        name: '危险的 HTML 插入指令：`v-html`',
        component: () => import('@/views/1-2.v-html')
      },
      {
        path: '1-3.v-for&v-if',
        name: '`v-for` 与 `v-if`',
        component: () => import('@/views/1-3.v-for&v-if')
      }
    ]
  },
  {
    name: '特殊属性',
    children: [
      {
        path: '2-1.key',
        name: '唯一的标识符 `key`',
        component: () => import('@/views/2-1.key')
      }
    ]
  },
  {
    name: '组件',
    children: [
      {
        path: '3-1.vue-file',
        name: '`.vue` 文件与 vue 的实例',
        component: () => import('@/views/3-1.vue-file')
      },
      {
        path: '3-2.mixins',
        name: '混入对象 mixins 选项',
        component: () => import('@/views/3-2.mixins')
      },
      {
        path: '3-3.provide',
        name: '依赖注入 provide/inject 选项',
        component: () => import('@/views/3-3.provide-inject')
      },
      {
        path: '3-4.event-hub',
        name: '事件中心',
        component: () => import('@/views/3-4.event-hub')
      }
    ]
  }
];

export default navs;

export const navsRoute = navs.reduce(
  (prev, curr) => [...prev, ...curr.children],
  []
);

/**
 * user token 操作
 */
import { Auth } from './auth.class';

export const userAuth = new Auth('USER_AUTH_TOKEN');

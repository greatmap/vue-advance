import parse from 'qs/lib/parse';

export default function getParam(key) {
  const query = location.href.replace(/.*\?/, '');
  const param = parse(query);
  return param[key];
}

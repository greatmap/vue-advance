export default {
  data() {
    return {
      state: false
    };
  },
  methods: {
    toggleActive(state = !this.state) {
      this.state = state;
    }
  }
};

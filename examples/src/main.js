import Vue from 'vue';
import App from './App.vue';

// 路由
import router from './router';

// 插件
import './plugins/element.js';

// 样式
import './styles/index.scss';

// 开发环境配置
if (process.env.NODE_ENV === 'development') {
  // mock 服务
  require('./mock');
}

Vue.config.productionTip = false;

new Vue({
  el: '#app',
  router,
  render: (h) => h(App)
});

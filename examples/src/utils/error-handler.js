/**
 * 错误信息提示
 * @param {String} message
 */
import Message from 'element-ui/packages/message';

export default function errorMessage(message) {
  // 打印出错误信息
  console.log('Error: ', message);

  // 显示错误信息
  Message({
    message,
    type: 'error',
    duration: 5000
  });
}

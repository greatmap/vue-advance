export default {
  data() {
    return {
      mapId: ''
    };
  },
  inject: ['getMap'],
  created() {
    this.$map = this.getMap();
    this.mapId = this.$map.id;
  }
};

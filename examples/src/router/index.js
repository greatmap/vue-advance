/**
 * 路由配置
 */
import Vue from 'vue';
import Router from 'vue-router';

// 主容器组件
import Layout from '../layout';
import { navsRoute } from '@/config/navs';

// 路由集合
const routes = [
  {
    path: '/',
    name: 'layout',
    component: Layout,
    redirect: navsRoute[0].path,
    children: navsRoute
  }
];

Vue.use(Router);

export default new Router({
  // mode: 'history',
  scrollBehavior: () => ({ x: 0, y: 0 }),
  routes
});

# 目录

[TOC]

# Vue 进阶

主要内容包括：

- 实现流程与原理
- 一些进阶知识点
- 相关技术的扩展
- 实践中的规范与建议

## 原理篇

### Vue 实例化流程

组件实例化的流程：

> 组件是可复用的 Vue 实例。

```js
import Vue from 'vue';

// Vue 实例化
const instance = new Vue();

// 生成 VueCompnent 类，它是 Vue 的一个子类
const Test = Vue.component('test', {});

// 在 instance 中实例化，最终也是 Vue 的实例化
// 可以在多个实例中进行组件的实例化
new Test(instance);
```

在 Vue 实例化的过程，到底发生了什么？

（看图 - Vue 实例化流程）

### 响应式原理

数据与视图是如何交互的？

#### 观察者模式

观察者模式（又被称为发布-订阅（Publish/Subscribe）模式，属于行为型模式的一种，
它定义了一种一对多的依赖关系，让多个观察者对象同时监听某一个主题对象。
这个主题对象在状态变化时，会通知所有的观察者对象，使他们能够自动更新自己。

```
class Producer {
    constructor() {
        this.subs = [];
    }

    addSub(sub) {
        this.subs.push(sub);
    }

    removeSub(sub) {
        this.subs.splice(this.subs.indexOf(sub), 1);
    }

    notify(message) {
        this.subs.forEach(
            sub => sub(message)
        )
    }
}

// 创建一个实例
const example = new Producer();

// 订阅
const sub1 = example.addSub(() => { doSomeThing1() });
const sub2 = example.addSub(() => { doSomeThing2() });

// 发布
example.notify('test');
```

> 观察者模式在这里的应用主要是处理视图对数据的变更进行订阅，数据的更新会发送通过给所有的订阅者。

#### Object.defineProperty

通过该方法定义的对象属性有两种类型：

```
// 数据属性
const o = {};
Object.defineProperty(o, 'test', {
    configuable: true, // 是否能够删除
    enumerable: true, // 是否能够枚举
    writable: true, // 是否能够修改
    value: 'test' // 初始值
});

// 访问器属性
const o = {};
Object.defineProperty(o, 'test', {
    configuable: true, // 是否能够删除
    enumerable: true, // 是否能够枚举
    get: function() {
        return 'test';
    }
    set: function(value) {
        return value;
    }
});
```

> `Object.defineProperty` 定义对象的访问器属性能够对数据的获取与设置进行监听，
> 为依赖收集与数据变更提供服务。

#### 依赖收集

主要是数据收集依赖它的视图（用于在数据更新时，触发依赖它的视图进行更新）。

实现技巧：

- 创建一个全局的变量 `Dep.target`
- 在视图获取数据时，将视图本身赋值给 `Dep.target = view`
- 在视图获取数据时，触发了数据的 `get` 方法
- 此时在数据中即接受到依赖的视图 `Dep.target`

### 虚拟 dom

> 一个简单的对象去代替复杂的 dom 对象。

#### 虚拟 node

数据结构：

```typescript
export interface VNode {
  tag?: string; // 标签名
  data?: VNodeData; // 数据对象，下面的 VNodeData 类型
  children?: VNode[]; // 子集
  text?: string; // 当前节点的文本内容
  elm?: Node; // 虚拟节点对应的真实 dom 节点
  ns?: string; // namespace 节点的命名空间
  context?: Vue; // 编译作用域
  key?: string | number; // 节点的 key 值，用于 diff 算法优化
  componentOptions?: VNodeComponentOptions; // 组件选项
  componentInstance?: Vue; // 组件的实例
  parent?: VNode; // 父级节点
  raw?: boolean; // html 字符串
  isStatic?: boolean; // 是否静态节点
  isRootInsert: boolean; // 是否根节点插入
  isComment: boolean; // 是否为注释
}

export interface VNodeData {
  key?: string | number; // 节点的 key 值，用于 diff 算法优化
  slot?: string; // 插槽内容
  scopedSlots?: { [key: string]: ScopedSlot | undefined }; // 插槽作用域
  ref?: string;
  refInFor?: boolean;
  tag?: string;
  staticClass?: string; // 静态样式类
  class?: any; // 样式类
  staticStyle?: { [key: string]: any }; // 静态内联样式
  style?: string | object[] | object; // 内联样式
  props?: { [key: string]: any }; // props
  attrs?: { [key: string]: any }; // attrs
  domProps?: { [key: string]: any };
  hook?: { [key: string]: Function };
  on?: { [key: string]: Function | Function[] };
  nativeOn?: { [key: string]: Function | Function[] };
  transition?: object;
  show?: boolean;
  inlineTemplate?: {
    render: Function;
    staticRenderFns: Function[];
  };
  directives?: VNodeDirective[];
  keepAlive?: boolean;
}
```

VNode 渲染实例：

```js
vnodeObj = {
  tag: 'div',
  children: [
    {
      tag: 'h1',
      data: {
        staticClass: 'title'
      },
      text: 'title'
    }
  ]
};
```

对应的模版：

```html
<div>
  <h1 class="title">title</h1>
</div>
```

VNode 的创建：`vm.$createElement` 方法（看图 - Vue 实例化流程）

> `vm.$createElement` 方法也是 `render` 函数的参数。
> 例如：main.js 中的 `render: (h) => h(App)`。

#### diff 算法

**diff 的比较只会在同层级进行，不会跨级。**（看图 - pathVnode 与 updateChildren）

## 知识点

### 指令

#### 文本插值的扩展：`v-once` 指令

`v-once` 指令使当前范围的内容只会初始化一次，没有后续的 DOM 更新。

> 这类似 angularjs 中的双冒号绑定。 `{{ ::value }}` 或者 `ng-bind="::value"`。

用法：

```vue
<template>
  <!-- 实例1：单个元素 -->
  <span v-once>{{ msg }}</span>

  <!-- 实例2：区域内 -->
  <div v-once>
    <span>{{ title }}</span>
    <span>{{ content }}</span>
  </div>
</template>

<script>
// 实例3：创建静态组件（缓存静态内容）
Vue.component('terms-of-service', {
  template: `
    <div v-once>
      <h1>Terms of Service</h1>
      ... a lot of static content ...
    </div>
  `
});
</script>
```

好处：

提高性能，特别是在大的列表与表格中，有些数据是不会更改的，只需渲染一次即可。

案例：

- 头部的 logo 区域
- 左侧的导航栏
- 底部的网站信息区域
- 静态列表（列表的数据不会发生更新的）

注意：

不要滥用，在更多的时候，如果你不清楚这个地方的需不需要更新，最好是不使用它，
因为如果后面这个地方被其他人修改了（恰好又是不了解这个指令的作用），
可能会出现无法找到数据未更新的原因的问题。

#### 危险的 HTML 插入指令：`v-html`

`v-html` 指令可以在模版中插入 html，**但是要注意一定不要将用户提交的 html 插入到模版中，
这样网站很容易遭到 XSS 攻击。**

扩展：

> Cross-Site Scripting（跨站脚本攻击）简称 XSS（避免与 CSS 混淆），是一种代码注入攻击。
> 攻击者通过在目标网站上注入恶意脚本，使之在用户的浏览器上运行。
> 利用这些恶意脚本，攻击者可获取用户的敏感信息如 Cookie、SessionID 等，进而危害数据安全。

XSS 实例：

有这样一个需求，搜索站点根据 URL 的参数决定页面上展示的关键词内容，如下。

```vue
<template>
  <div>您搜索的关键词是：<span v-html="keyword"></span></div>
</template>

<script>
export default {
  data() {
    return {
      // 从 URL 中获取这个 keyword 对应的内容
      keyword: getParam('keyword')
    };
  }
};
</script>
```

**此时当恶意用户给你发送一个链接
`https://xxx.xxx.com?keyword=<a href="javascript:alert('XSS')">点击领红包</a>` 时，
当你打开时便受到了攻击，你的信息可能就被窃取了。**

如何防范：

将 `&，<，>，"，'，/` 等危险符号进行替换为 html 的符号代码（例如：`< 替换为 &amp;`）。

> 参考文章：《[美团前端安全系列（一）：如何防止 XSS 攻击](https://segmentfault.com/a/1190000016551188)》

#### `v-for` 与 `v-if`

**永远不要把 `v-for` 与 `v-if` 用在同一个元素上。**

为何：`v-for` 优先级大于 `v-if`，`v-for` 先执行，遍历了整个列表，才过滤出 `v-if` 的条件。

实例 - 同时使用进行过滤：

```vue
<template>
  <ul>
    <li v-for="user in users" v-if="user.isActive" :key="user.id">
      {{ user.name }}
    </li>
  </ul>
</template>

<script>
export default {
  data() {
    return {
      users: []
    };
  }
};
</script>
```

这样写的问题：

1. 展示的数组受两个变量控制，任何一个改变都会导致列表的重新遍历（即使不是当前展示的列表也会导致重新遍历）
2. 不利于扩展与修改（比如：增加/修改过滤的条件）

优化：

1. 如果可以，将 `v-if` 移至外层
2. 将过滤逻辑修改为计算属性

修改后：

```vue
<template>
  <ul>
    <li v-for="user in activeUsers" :key="user.id">
      {{ user.name }}
    </li>
  </ul>
</template>

<script>
export default {
  data() {
    return {
      users: []
    };
  },
  computed: {
    activeUsers() {
      return this.users.filter(function(user) {
        return user.isActive;
      });
    }
  }
};
</script>
```

好处：

- 列表的更新只受一个变量的影响，而且这个就是要展示的数据，提高效率
- 列表只是展示 `v-for` 的内容，不再有过滤的逻辑，解藕逻辑，提高维护性

### 特殊属性

#### 唯一的标识符 `key`

`key` 的特殊属性主要用在 Vue 的**虚拟 DOM 算法**，在新旧 nodes 对比时辨识 VNodes。

1. 如果不使用 key，Vue 会使用一种**最大限度减少动态元素并且尽可能的尝试修复/再利用相同类型元素**的算法。
2. 使用 key，它会**基于 key 的变化重新排列元素顺序，并且会移除 key 不存在的元素**。
3. 有相同父元素的的子元素必须有**独特的 key**。重复的 key 会造成渲染错误。

- 实例：`v-for` 与 `transition-group`

列表组件：

```vue
<template>
  <div>
    <ListItem v-for="item in data" :data="item" :key="item" />
    <el-divider></el-divider>
    <el-button @click="insert()">Insert</el-button>
  </div>
</template>

<script>
import ListItem from './list-item';

export default {
  components: {
    ListItem
  },
  data() {
    return {
      data: Array(10)
        .fill('')
        .map((i, index) => index)
    };
  },
  methods: {
    insert() {
      this.data.splice(3, 0, this.data.length);
    }
  }
};
</script>
```

列表内容组件：

```vue
<template>
  <button>{{ data }}</button>
</template>

<script>
export default {
  props: {
    data: Number
  }
};
</script>
```

（看图 - diff 算法）

- 为何使用 `key`：
  - 高效的更新 DOM
  - 使元素的动效正常触发
  - 避免边界情况（DOM 被移除，再重新添加后导致的问题）

#### 在模版中使用 JavaScript 表达式的建议

如果表达式很简单，例如 `@click="number + 1"`，此时建议直接在模版中写。
如果表达式稍长，较复杂，务必写成计算属性。例如：

```vue
<template>
  {{
    message
      .split('')
      .reverse()
      .join('')
  }}
</template>
```

为何：

- 提高可读性与维护性，模版上不易阅读与更改
- 模版上无法调试

### 组件篇

#### `.vue` 文件与 vue 的实例

`.vue` 文件只是在 webpack 的 `vue-loader` 加载器进行了如下编译：

1. 提取 template, js, style 内容
2. 将 style 以 `<style></style>` 标签插入 head 中
3. 编译 template 与 js 为 Vue 构造函数的选项（例如：渲染函数，data）
4. 最后创建 Vue 的实例（组件）

#### Prop 类型验证

**坚持对组件的 Prop 定义类型（或者更详细的验证）。**

为何：

- 它们写明了组件的 API，所以很容易看懂组件的用法；
- 在开发环境下，如果向一个组件提供格式不正确的 prop，Vue 将会告警，以帮助你捕获潜在的错误来源

常用验证类型：

```js
Vue.component('my-component', {
  props: {
    // 基础的类型检查 (`null` 和 `undefined` 会通过任何类型验证)
    propA: Number,
    // 多个可能的类型
    propB: [String, Number],
    // 必填的字符串
    propC: {
      type: String,
      required: true
    },
    // 带有默认值的数字
    propD: {
      type: Number,
      default: 100
    },
    // 带有默认值的对象
    propE: {
      type: Object,
      // 对象或数组默认值必须从一个工厂函数获取
      default: function() {
        return { message: 'hello' };
      }
    },
    // 自定义验证函数
    propF: {
      validator: function(value) {
        // 这个值必须匹配下列字符串中的一个
        return ['success', 'warning', 'danger'].indexOf(value) !== -1;
      }
    }
  }
});
```

#### 优雅的计算属性

**计算属性是基于它们的响应式依赖进行缓存的。（看图 - Vue 实例化流程）**

无论是性能还是书写便捷性上都是最优的。
看下面的实例。

- 代替方法与监听

```vue
<script>
export default {
  props: {
    data: Array
  },
  data() {
    return {
      chunkData: []
    };
  },
  methods: {
    getChunkData() {
      return chunk(this.data, 3);
    }
  },
  watch: {
    data() {
      this.chunkData = this.getChunkData();
    }
  },
  // 如果用计算属性，就很简单了
  computed: {
    chunkData() {
      return chunk(this.data, 3);
    }
  }
};
</script>
```

#### 组件的复用

> 重复是万恶之源。

##### 混入对象 mixins 选项

混入 (mixin) 提供了一种非常灵活的方式，来分发 Vue 组件中的可复用功能。
一个混入对象可以包含任意组件选项。当组件使用混入对象时，
所有混入对象的选项将被“混合”进入该组件本身的选项。

实例（状态，方法，生命周期的复用）：

```js
const toggleActiveMixin = {
  data() {
    return {
      state: false
    };
  },
  methods: {
    toggleActive(state = !this.state) {
      this.state = state;
    }
  }
};

const lifecycleMixin = {
  beforeCreate() {
    console.log('beforeCreate ->', this);
  },
  created() {
    console.log('created ->', this);
  },
  beforeMount() {
    console.log('beforeMount ->', this);
  },
  mounted() {
    console.log('mounted ->', this);
  }
};

// componentA
new Vue({
  mixins: [toggleActiveMixin, lifecycleMixin]
});

// componentB
new Vue({
  mixins: [toggleActiveMixin, lifecycleMixin]
});
```

#### 组件的交互

两种跨组件之间的交互方式。

> （这两种方式可能不常用）

##### 依赖注入 provide/inject 选项

> provide 和 inject 主要为高阶插件/组件库提供用例。并不推荐直接用于应用程序代码中。

这对选项需要一起使用，以允许一个祖先组件向其所有子孙后代注入一个依赖，
不论组件层次有多深，并在起上下游关系成立的时间里始终生效。

参数类型：

- provide: `Object | () => Object`
- inject: `Array<string> | { [key: string]: string | Symbol | Object }`

> provide 和 inject 绑定并不是可响应的。（看图 - Vue 实例化流程）

实例（地图实例化对象在组件间的传递）：

地图组件：

```vue
<template>
  <el-button v-loading="loading">
    <h2>map（mapId: {{ mapId }}）</h2>
    <MapControls v-if="!loading"></MapControls>
  </el-button>
</template>

<script>
import MapControls from './map-controls';

export default {
  components: {
    MapControls
  },
  provide() {
    return {
      getMap: this.getMap
    };
  },
  data() {
    return {
      mapId: '',
      loading: true
    };
  },
  methods: {
    getMap() {
      return this.$map;
    }
  },
  mounted() {
    // 模拟 map 加载完毕
    setTimeout(() => {
      this.$map = { id: 'map1' };
      this.mapId = this.$map.id;
      this.loading = false;
    }, 1000);
  }
};
</script>

<style lang="scss" scoped></style>
```

地图里面的控制层组件：

```vue
<template>
  <el-button>
    <h3>map-controls（mapId: {{ mapId }}）</h3>
    <MapControlsBtn></MapControlsBtn>
  </el-button>
</template>

<script>
// import mapInjectMixin from '../mixins/map-inject';
import MapControlsBtn from './map-controls-btn';

export default {
  // mixins: [mapInjectMixin],
  data() {
    return {
      mapId: ''
    };
  },
  components: {
    MapControlsBtn
  },
  inject: ['getMap'],
  created() {
    this.$map = this.getMap();
    this.mapId = this.$map.id;
  }
};
</script>

<style lang="scss" scoped></style>
```

控制层内的按钮组件：

```vue
<template>
  <div>
    <el-button>map-controls-btn（mapId: {{ mapId }}）</el-button>
  </div>
</template>

<script>
// import mapInjectMixin from '../mixins/map-inject';

export default {
  // mixins: [mapInjectMixin],
  data() {
    return {
      mapId: ''
    };
  },
  inject: ['getMap'],
  created() {
    this.$map = this.getMap();
    this.mapId = this.$map.id;
  }
};
</script>

<style lang="scss" scoped></style>
```

##### 事件中心

跨组件间的事件交互。

创建事件中心：

```js
import Vue from 'vue';

export default new Vue();
```

子组件 1/2 使用事件中心：

```vue
<template>
  <div>
    <el-button @click="add()">example1 count: {{ count }}</el-button>
  </div>
</template>

<script>
import eventHub from '../event-hub';

export default {
  data() {
    return {
      count: 0
    };
  },
  methods: {
    add() {
      this.count++;
      eventHub.$emit('countChange', this.count);
    },
    countChange(count) {
      this.count = count;
    }
  },
  created() {
    eventHub.$on('countChange', this.countChange);
  },
  beforeDestroy() {
    eventHub.$off('countChange', this.countChange);
  }
};
</script>

<style lang="scss" scoped></style>
```

> 注意，在组件销毁前务必销毁事件监听，因为事件还存在事件中心中不会移除。

> over

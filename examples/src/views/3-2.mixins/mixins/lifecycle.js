export default {
  beforeCreate() {
    console.log('beforeCreate ->', this);
  },
  created() {
    console.log('created ->', this);
  },
  beforeMount() {
    console.log('beforeMount ->', this);
  },
  mounted() {
    console.log('mounted ->', this);
  }
};
